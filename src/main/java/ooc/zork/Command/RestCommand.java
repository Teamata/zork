package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;

/**
 * Created by Teama on 1/29/2018.
 */
public class RestCommand implements Command {
    @Override
    public void apply() {
        Status.setMove(false);
        if(App.world.getCurrentLevel().getStartingRoom().equals(App.world.getCurrentLevel().getCurrentRoom())) {
            System.out.println("You took a rest, your hp has been restored to full! but you also feel a strange presence that was once gone");
            App.world.getCurrentLevel().resetLevel();
            App.player.setCurrentHP(App.player.getMaxHP());
        }
        else{
            System.out.println("It's too uncomfortable to rest here!");
        }
    }

    @Override
    public String getDescription() {
        return "Rest - taking a rest will restore your health to full, however can only be use in the room which has a bonfire ";
    }
}
