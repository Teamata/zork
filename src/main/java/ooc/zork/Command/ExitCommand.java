package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;

import java.util.Scanner;

/**
 * Created by Teama on 1/26/2018.
 */
public class ExitCommand implements Command {
    Scanner scanner = new Scanner(System.in);
    @Override

    public void apply() {
        System.out.println("Are you sure you want to quit the game? [Y/N]");
        String argument = scanner.next().toLowerCase();
        if(argument.equals("y")) {
            System.out.println("Exiting the game");
            App.setQuit(true);
            Status.setUsingBag(false);
            Status.setBattlePhase(false);
        }
        else{
            System.out.println("Returning back to the game....");
        }
    }

    @Override
    public String getDescription() {
        return "Exit - exit the game.";
    }
}
