package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Command.Bag.*;
import ooc.zork.Command.Fight.AttackCommand;
import ooc.zork.Command.Fight.FightCommand;
import ooc.zork.Command.Fight.MonsterInfoCommand;
import ooc.zork.Status;

import java.util.HashMap;

/**
 * Created by Teama on 1/25/2018.
 */

public class CommandFactory {

    private static final HashMap<String, Command> commands = new HashMap<String, Command>() {
        {
            // commands are added here using lambdas. It is also possible to dynamically add commands without editing the code.
            put("up",new UpCommand());
            put("down",new DownCommand());
            put("left",new LeftCommand());
            put("right", new RightCommand());
            put("talk", new TalkCommand());
            put("exit", new ExitCommand());
            put("help", new HelpCommand());
            put("inspect",new InspectCommand());
            put("info", new InfoCommand());
            put("fight", new FightCommand());
            put("bag", new BagCommand());
            put("take",new TakeCommand());
            put("rest", new RestCommand());
            put("attack", new FightCommand());
            put("map", new MapCommand());
        }
    };

    private static final HashMap<String, Command> fightCommands = new HashMap<String, Command>(){
        {
            put("attack", new AttackCommand());
            put("fight", new AttackCommand());
            put("help", new HelpCommand());
            put("exit", new ExitCommand());
            put("info", new InfoCommand());
            put("talk", new TalkCommand());
            put("bag" , new BagCommand());
            put("monsterinfo",new MonsterInfoCommand());
        }
    };

    private static final HashMap<String, Command> bagCommands= new HashMap<String, Command>(){
        {
            put("drop", new DropCommand());
            put("equip", new EquipCommand());
            put("use", new UseItemCommand());
            put("unequip", new UnEquipCommand());
            put("back",new ExitbagCommand());
            put("info", new InfoCommand());
            put("exit", new ExitCommand());
        }
    };


    public static Command getCommand(String name) {
        return commands.get(name);
    }

    public static Command getFightCommand(String name){
        return fightCommands.getOrDefault(name,null);
    }

    public static Command getBagCommand(String name) { return bagCommands.getOrDefault(name,null);}

    public static void printCommand(){
        StringBuilder printing = new StringBuilder();
        printing.append("Available command : ");
        if(Status.getUsingBag()){
            printing.append(" drop back equip use unequip info exit");
        }
        else if(Status.getBattlePhase()){
            printing.append(" attack bag help info monsterinfo talk exit");
        }
        //walking
        else {
            printing.append(" up down left right inspect help info bag map");
            if (App.world.getCurrentLevel().getCurrentRoom().hasNPC()) {
                printing.append(" talk");
            }
            if (App.world.getCurrentLevel().getCurrentRoom().hasItem()) {
                printing.append(" take");
            }
            if (App.world.getCurrentLevel().getCurrentRoom().hasMonster()) {
                printing.append(" fight");
            }
            if(App.world.getCurrentLevel().getCurrentRoom().equals(App.world.getCurrentLevel().getStartingRoom())){
                printing.append(" rest");
            }
        }
        System.out.println(printing.toString());
    }

}
