package ooc.zork.Command;

import ooc.zork.Command.Bag.*;
import ooc.zork.Command.Fight.AttackCommand;
import ooc.zork.Command.Fight.FightCommand;
import ooc.zork.Command.Fight.MonsterInfoCommand;
import ooc.zork.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teama on 1/25/2018.
 */
public class HelpCommand implements Command{

    private static List<Command> commands = new ArrayList<>();

    static{
        commands.add(new AttackCommand());
        commands.add(new BagCommand());
        commands.add(new ExitbagCommand());
        commands.add(new DownCommand());
        commands.add(new DropCommand());
        commands.add(new EquipCommand());
        commands.add(new FightCommand());
        commands.add(new HelpCommand());
        commands.add(new InfoCommand());
        commands.add(new InspectCommand());
        commands.add(new LeftCommand());
        commands.add(new MapCommand());
        commands.add(new MonsterInfoCommand());
        commands.add(new RestCommand());
        commands.add(new RightCommand());
        commands.add(new TakeCommand());
        commands.add(new TalkCommand());
        commands.add(new UpCommand());
        commands.add(new UnEquipCommand());
        commands.add(new UseItemCommand());
    }


    @Override
    public void apply() {
        Status.setMove(false);
        StringBuilder print = new StringBuilder();
        for(int i = 0; i<commands.size();i++){
            print.append(commands.get(i).getDescription()).append("\n");
        }
        System.out.println(print.toString());
    }

    @Override
    public String getDescription() {
        return "Help - lists all available commands";
    }

}
