package ooc.zork.Command.Fight;

import ooc.zork.App;
import ooc.zork.Command.Command;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Status;

public class FightCommand implements Command {
    @Override
    public void apply() {
        Status.setMove(false);
        if(App.world.getCurrentLevel().getCurrentRoom().hasMonster()) {
            Status.setBattlePhase(true);
        }
        else{
            System.out.println("There is nothing to battle against!");
        }
    }

    @Override
    public String getDescription() {
        return "Fight - start a battle with a monster, doing so will enter a battle phase. Can only be use when there's a monster in the room";
    }
}
