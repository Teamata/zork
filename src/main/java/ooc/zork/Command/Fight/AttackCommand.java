package ooc.zork.Command.Fight;

import ooc.zork.App;
import ooc.zork.Combat;
import ooc.zork.Command.Command;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Status;

public class AttackCommand implements Command {

    public void apply() {
        Status.setMove(false);
        Monster monster = App.world.getCurrentLevel().getCurrentRoom().getMonster();
        Combat.fight(monster,App.player);
        Status.setAttack(true);
    }

    @Override
    public String getDescription() {
        return "Attack - attack a monster, can only be use during battle";
    }
}
