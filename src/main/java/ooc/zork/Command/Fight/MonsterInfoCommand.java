package ooc.zork.Command.Fight;

import ooc.zork.App;
import ooc.zork.Command.Command;
import ooc.zork.Status;

/**
 * Created by Teama on 1/29/2018.
 */
public class MonsterInfoCommand implements Command {
    @Override
    public void apply() {
        Status.setMove(false);
        Status.setAttack(true);
        if(Status.getBattlePhase()){
            System.out.println("An enemy stats");
            App.world.getCurrentLevel().getCurrentRoom().getMonster().printStats();
        }
    }

    @Override
    public String getDescription() {
        return "Monsterinfo - display status of a monster. Can only be use during a battle phase";
    }
}
