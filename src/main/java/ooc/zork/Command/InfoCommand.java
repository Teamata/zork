package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;

import java.util.List;
import java.util.Scanner;

/**
 * Created by Teama on 1/27/2018.
 */
public class InfoCommand implements Command {

    @Override
    public void apply() {
        Status.setMove(false);
        Status.setAttack(true);
        if(Status.getBattlePhase()){
            System.out.println("An enemy stats");
            App.world.getCurrentLevel().getCurrentRoom().getMonster().printStats();
        }
        else if(Status.getUsingBag()){
            System.out.println("Please select an item to check");
            String temp = new Scanner(System.in).next();
            if (!temp.equals("back")) {
                App.player.getBag().getItem(Integer.parseInt(temp) - 1).getDescription();
            }
            else{
                System.out.println("Please input a proper number, if you wish to exit just type 'back'");
            }
        }
        else{
            App.player.printStats();
        }
    }

    @Override
    public String getDescription() {
        return "Info - check player status or item";
    }
}
