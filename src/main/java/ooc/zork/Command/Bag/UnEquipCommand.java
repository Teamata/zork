package ooc.zork.Command.Bag;

import ooc.zork.App;
import ooc.zork.Command.Command;
import ooc.zork.Command.CommandFactory;
import ooc.zork.Status;

import java.util.Scanner;

/**
 * Created by Teama on 1/28/2018.
 */
public class UnEquipCommand implements Command {

    Scanner scanner = new Scanner(System.in);

    @Override
    public void apply() {
        System.out.println("You decided to unequip an item please select what item's number that you want to unequip");
        while(true) {
            String itemNum = scanner.next();
            if(itemNum.equals("back")){
                CommandFactory.getBagCommand(itemNum).apply();
                break;
            }
            else{
                try {
                    int index = Integer.valueOf(itemNum);
                    App.player.unequip(index);
                    break;
                }
                catch(NumberFormatException e){
                    System.out.println("Please input a proper number, if you wish to exit just type 'back'");
                }
            }
        }
        Status.setAttack(false);
        App.player.getBag().printInventory();
    }

    @Override
    public String getDescription() {
        return "Unequip - unequip an item, choosing by selecting a specified slot number. Can only be use when you're using bag";
    }
}
