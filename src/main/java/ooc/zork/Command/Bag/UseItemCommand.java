package ooc.zork.Command.Bag;

import ooc.zork.App;
import ooc.zork.Command.Command;
import ooc.zork.Command.CommandFactory;
import ooc.zork.Status;

import java.util.Scanner;

/**
 * Created by Teama on 1/28/2018.
 */
public class UseItemCommand implements Command {

    Scanner scanner = new Scanner(System.in);

    @Override
    public void apply() {
        System.out.println("You decided to use item please select what item's number that you want to use");
        while(true) {
            String itemNum = scanner.next();
            if(itemNum.equals("back")){
                CommandFactory.getBagCommand(itemNum).apply();
                break;
            }
            else{
                try {
                    int index = Integer.valueOf(itemNum);
                    App.player.useItem(index);
                    break;
                }
                catch(NumberFormatException e){
                    System.out.println("Please input a proper number, if you wish to exit just type 'back'");
                }
            }
        }
        if(Status.getBattlePhase()) {
            Status.setAttack(false);
        }
        else{
            //Player still use the bag.
            App.player.getBag().printInventory();
        }
    }

    @Override
    public String getDescription() {
        return "Use - use an item. Can only be use when you're using bag";
    }
}
