package ooc.zork.Command.Bag;

import ooc.zork.App;
import ooc.zork.Command.Command;
import ooc.zork.Status;

import java.util.Scanner;

/**
 * Created by Teama on 1/28/2018.
 */
public class BagCommand implements Command {
    @Override
    public void apply() {
        System.out.println("You open your bag");
        App.player.getBag().printInventory();
        Status.setUsingBag(true);
        Status.setAttack(false);
    }

    @Override
    public String getDescription() {
        return "Bag - check your inventory, can also be use during battle in return of losing a turn";
    }


}
