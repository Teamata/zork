package ooc.zork.Command.Bag;

import ooc.zork.Command.Command;
import ooc.zork.Status;

/**
 * Created by Teama on 1/28/2018.
 */
public class ExitbagCommand implements Command {
    @Override
    public void apply() {
        System.out.println("you put your bag away");
        Status.setUsingBag(false);
    }

    @Override
    public String getDescription() {
        return "Back - can be use when you use an inventory and you want to put it away";

    }
}
