package ooc.zork.Command;
import ooc.zork.App;

/**
 * Created by Teama on 1/30/2018.
 */
public class MapCommand implements Command {
    @Override
    public void apply() {
        App.world.getCurrentLevel().displayLevel();
        if (!App.world.getCurrentLevel().getFoundMap()) {
            System.out.println("You use a reminiscence of your memory to try to remember the path you took");
        }
        else{
            System.out.println("You bring out your map");
        }
    }

    @Override
    public String getDescription() {
        return "Map - display a map with fog of war, if you hold a map of this floor then it'll be fully revealed";
    }
}
