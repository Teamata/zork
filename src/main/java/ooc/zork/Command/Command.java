package ooc.zork.Command;


/**
 * Created by Teama on 1/25/2018.
 */
public interface Command {

    void apply();

    String getDescription();

}
