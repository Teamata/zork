package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;

/**
 * Created by Teama on 1/26/2018.
 */
public class InspectCommand implements Command {
    @Override
    public void apply() {
        System.out.println("You're at X,Y:" + App.player.getPositionX() +" "+ App.player.getPositionY());
        App.world.getCurrentLevel().getCurrentRoom().lookAround();
        Status.setMove(false);
    }

    @Override
    public String getDescription() {
        return "Inspect - check what does the room contain";
    }
}
