package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;
import ooc.zork.World.Level;

/**
 * Created by Teama on 1/25/2018.
 */

public class UpCommand implements Command{

    @Override
    public void apply() {
        if(App.world.getCurrentLevel().validGrid(App.player.getPositionX()-1,App.player.getPositionY()) &&
                App.world.getCurrentLevel().getHasRoom()[App.player.getPositionX() - 1][App.player.getPositionY()]) {
                App.player.setPositionX(App.player.getPositionX() - 1);
                Status.setMove(true);
            }
        else{
            Status.setMove(false);
            System.out.println("That's not the way, or perhaps your mind are shroud by darkness");
        }

    }

    @Override
    public String getDescription() {
        return "Up - go up";
    }

}
