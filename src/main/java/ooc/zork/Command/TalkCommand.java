package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;

/**
 * Created by Teama on 1/25/2018.
 */
public class TalkCommand implements Command{
    @Override
    public void apply() {
        if(Status.getBattlePhase()){
            System.out.println("You try to talk, but it doesn't understand you!");
        }
        else{
            App.world.getCurrentLevel().getCurrentRoom().getNPC().talk();
        }
    }

    @Override
    public String getDescription() {
        return "Talk - talk to a Non-Playable-Character(NPC)";
    }
}
