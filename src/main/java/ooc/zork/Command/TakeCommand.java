package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Items.Item;

/**
 * Created by Teama on 1/25/2018.
 */
public class TakeCommand implements Command{
    @Override
    public void apply() {
        if(App.world.getCurrentLevel().getCurrentRoom().hasItem()){
            Item temp = App.world.getCurrentLevel().getCurrentRoom().getItem();
            if(App.player.pickUp(temp)){
                App.world.getCurrentLevel().getCurrentRoom().removeItem();
            }
        }
        else {
            System.out.println("There is nothing to take!");
        }
    }

    @Override
    public String getDescription() {
        return "Take - take an item that's in this room";
    }
}
