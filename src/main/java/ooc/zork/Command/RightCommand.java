package ooc.zork.Command;

import ooc.zork.App;
import ooc.zork.Status;
import ooc.zork.World.Level;

/**
 * Created by Teama on 1/25/2018.
 */
public class RightCommand implements Command{
    @Override
    public void apply() {
        if(App.world.getCurrentLevel().validGrid(App.player.getPositionX(),App.player.getPositionY()+1) &&
        App.world.getCurrentLevel().getHasRoom()[App.player.getPositionX()][App.player.getPositionY() + 1]) {
            App.player.setPositionY(App.player.getPositionY() + 1);
            Status.setMove(true);
        } else {
            Status.setMove(false);
            System.out.println("That's not the way, or perhaps your mind are shroud by darkness");
        }
    }

    @Override
    public String getDescription() {
        return "Right - go right";
    }

}
