package ooc.zork.Objectives;

import ooc.zork.App;
import ooc.zork.World.Level;

/**
 * Created by Teama on 1/27/2018.
 */
public class ClearAllMonster implements Objective {

    private boolean questCleared;
    ClearAllMonster(){
        questCleared = false;
    }

    @Override
    public void getDescription() {
        System.out.println("OBJECTIVE! :  You must kill all monsters in this floor");
    }


    @Override
    public boolean isCleared() {
        if(App.world.getCurrentLevel().getMonsterKilled() >= App.world.getCurrentLevel().getMonsterCount()){
            questCleared = true;
        }
        return questCleared;
    }
}
