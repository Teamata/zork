package ooc.zork.Objectives;

import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.NPC.NPC;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Teama on 1/27/2018.
 */

public class ObjectiveFactory {

    final static Random rand = new Random();

    private static Map<Integer, Class<? extends Objective>> npcQuests
            = new HashMap<Integer, Class<? extends Objective>>() {
        {
//            put(0, FindItem.class);
            put(0, ClearAllMonster.class);
        }
    };

    private static Map<Integer, Class<? extends Objective>> bossQuests
            = new HashMap<Integer, Class<? extends Objective>>() {
        {
            put(1, KillSpecifiedMonster.class);
        }
    };

    public static Objective getObjective(boolean quest, int level) {
        Objective q = null;
        try {
            if (quest) {
                q = npcQuests.get(rand.nextInt(npcQuests.size())).newInstance();
            } else {
                q = bossQuests.get(1).newInstance();
            }
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return q;
    }
}
