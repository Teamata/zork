package ooc.zork.Objectives;

import ooc.zork.World.Level;

/**
 * Created by Teama on 1/26/2018.
 */
public interface Objective {


    void getDescription();
    boolean isCleared();

}
