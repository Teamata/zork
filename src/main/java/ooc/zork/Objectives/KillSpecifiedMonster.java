package ooc.zork.Objectives;

import ooc.zork.App;

/**
 * Created by Teama on 1/26/2018.
 */

public class KillSpecifiedMonster implements Objective {

    private boolean questCleared;

    KillSpecifiedMonster(){
        questCleared = false;
    }
    @Override
    public void getDescription() {
        System.out.println("You must kill the boss monster of this floor!");
    }

    @Override
    public boolean isCleared() {
        if(!App.world.getCurrentLevel().getObjectiveRoom().hasMonster()){
            questCleared = true;
        }
        return questCleared;
    }

}
