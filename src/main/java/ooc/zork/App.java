package ooc.zork;

import ooc.zork.Command.*;
import ooc.zork.Entity.Protagonist.Protagonist;
import ooc.zork.World.World;

import java.util.Scanner;

public class App {

    public static Protagonist player;
    public static World world;
    static boolean quit = false;
    public static Command command;
    private static String commandLine;
    private static Scanner scanner = new Scanner(System.in);

    private static void creatingPlayer(){
        player = new Protagonist();
        System.out.println("Unknown voice : .... what an unfamiliar face we have here.. maybe .. no.. you're not");
        System.out.println("Unknown voice : It can't possibly be you out of thousands of people. Ah... pardon me");
        System.out.println("Unknown voice : What could your name be? if you're kind enough to tell me so..");
        System.out.println("Enter your name here : ");
        commandLine = scanner.nextLine();
        player.setName(commandLine);
    }

    private static void welcomeText(){
        System.out.println("Creating the chosen one...");
        System.out.println("Setting up map.........");
        System.out.println("Setting up levels.............");
    }

    private static void creatingWolrd(){
        world = new World();
        world.setUpWorld();
    }


    static void start() {
        welcomeText();
        creatingPlayer();
        creatingWolrd();
        world.getCurrentLevel().displayLevel();
        System.out.println("Game has started");
        System.out.println("As a fellow adventurer, one should not miss an opportunity to explore such mysterious place");
        System.out.println("Despite rumors of extreme danger that could cost one's life. You, " + App.player.getName() + " decides to step your foot in");
        System.out.println("There's no turning back now. Good luck");
        System.out.println("-------------------------------------------------------------------");
        while (!quit) {
            CommandFactory.printCommand();
            System.out.print(player.getName() + ": ");
            commandLine = scanner.nextLine().toLowerCase();
            command = CommandFactory.getCommand(commandLine);
            if (null == command) {
                System.out.println("Unknown command!");
            } else {
                command.apply();
                try {
                    Status.update();
                }
                catch(PlayerNotFoundException e){
                    System.err.println("Exception in thread game java.lang.IllegalPlayerState");
                    System.err.println("at ooc.zork.Status.playerDied(Status.java:20)");
                    System.err.println("Exception in thread game java.lang.PlayerNotFoundException");
                    System.err.println("Can't retrieve player information! player not found!");
                    System.err.println("Exiting the game...................................");
                    quit = true;
                }
                System.out.println("-------------------------------------------------------------------");
            }
        }
    }

    public static void setQuit(boolean quit) {
        App.quit = quit;
    }
}
