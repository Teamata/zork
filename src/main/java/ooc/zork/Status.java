package ooc.zork;

import ooc.zork.Command.Command;
import ooc.zork.Command.CommandFactory;

import java.util.Scanner;

/**
 * Created by Teama on 1/25/2018.
 */

public class Status {

    private static boolean battlePhase;
    private static boolean attack;
    private static boolean move;
    private static Scanner scanner = new Scanner(System.in);
    private static boolean usingBag;


    private static void playerDied() throws PlayerNotFoundException {
//        CommandFactory.getCommand("exit").apply();
        throw new PlayerNotFoundException();
    }

    private static void enteringNewLevel(){
        System.out.println("Unknown voice : You've passed the test, such challenge is a cake walk isn't it?");
        System.out.println("Unknown voice : I still expect more from you, adventurer..");
        System.out.println("You advanced to floor " + App.world.getCurrentLevel().getLevel() +" !");
    }

    private static void gameCleared(){
        System.out.println("Congratulations! " + App.player.getName() + " has cleared the game!");
    }

    private static void checkBattlephase() throws PlayerNotFoundException {
        if(battlePhase){
            System.out.println("Entering battle phase!");
            while(battlePhase){
                System.out.println("-------------------------------------------------------------------");
                CommandFactory.printCommand();
                //If boss monster, don't display HP
                if(App.world.getCurrentLevel().getCurrentRoom().getMonster().isBossMonster()){
                    System.out.println("Enemy HP : ???/???");
                }
                else{
                    System.out.println("Enemy HP : " + App.world.getCurrentLevel().getCurrentRoom().getMonster().getCurrentHP()+"/"+App.world.getCurrentLevel().getCurrentRoom().getMonster().getMaxHP());
                }
                System.out.println(App.player.getName() + " HP : " + App.player.getCurrentHP()+"/"+App.player.getMaxHP());
                System.out.print(App.player.getName() + ": ");
                String commandLine = scanner.nextLine().toLowerCase();
                Command command = CommandFactory.getFightCommand(commandLine);
                if(command!=null) {
                    command.apply();
                    checkUsingBag();
                    //do sth else beside attack
                    if (!attack) {
                        Combat.attack(App.world.getCurrentLevel().getCurrentRoom().getMonster(), App.player);
                    }
                    if (App.world.getCurrentLevel().getCurrentRoom().getMonster().isDead()) {
                        App.player.gainEXP(App.world.getCurrentLevel().getCurrentRoom().getMonster().getExp());
                        if(!App.world.getCurrentLevel().getCurrentRoom().canLeave()){
                            App.world.getCurrentLevel().getCurrentRoom().setCanLeave(true);
                        }
                        setBattlePhase(false);
                    }
                }
                //illegal command Player receive penalty
                else{
                    System.out.println("You didn't attack!!");
                    Combat.attack(App.world.getCurrentLevel().getCurrentRoom().getMonster(), App.player);
                }
                if(App.player.isDead()){
                    System.out.println("You died!");
                    playerDied();
                    break;
                }
            }
        }
    }

    private static void checkUsingBag(){
        if(Status.usingBag){
            while(usingBag){
                CommandFactory.printCommand();
                System.out.print(App.player.getName() + ": ");
                String commandLine = scanner.nextLine().toLowerCase();
                Command command = CommandFactory.getBagCommand(commandLine);
                if(command!=null){
                    command.apply();
                }
                else{
                    System.out.println("If you wish to exit just type 'back'");
                }
            }
        }
    }

    private static void checkMove() throws PlayerNotFoundException {
        if(Status.move){
            App.world.getCurrentLevel().addVisited();
            App.world.getCurrentLevel().displayLevel();
            App.world.getCurrentLevel().getCurrentRoom().getDescription();
            if(App.world.getCurrentLevel().getCurrentRoom().isTheEndRoom()){
                App.world.getCurrentLevel().getObjective();
            }
            if(App.world.getCurrentLevel().getCurrentRoom().hasMonster() && !App.world.getCurrentLevel().getCurrentRoom().canLeave()){
                System.out.println("The monster saw you!!");
                setBattlePhase(true);
                checkBattlephase();
            }
        }
    }

    static void update() throws PlayerNotFoundException {
        if(!App.quit) {
            if(App.world.getCurrentLevel().isClear()){
                if(App.world.nextLevel()) {
                    enteringNewLevel();
                    App.player.setPositionX(App.world.getCurrentLevel().getStartingX());
                    App.player.setPositionY(App.world.getCurrentLevel().getStartingY());
                }
                else{
                    gameCleared();
                    App.setQuit(true);
                }
            }
            checkBattlephase();
            checkUsingBag();
            checkMove();
        }
    }

    public static void setAttack(boolean attack) {
        Status.attack = attack;
    }

    public static void setMove(boolean move) {
        Status.move = move;
    }

    public static void setUsingBag(boolean usingBag) {
        Status.usingBag = usingBag;
    }

    public static void setBattlePhase(boolean battlePhase) {
        Status.battlePhase = battlePhase;
    }

    public static boolean getUsingBag(){
        return usingBag;
    }

    public static boolean getAttack(){
        return attack;
    }

    public static boolean getBattlePhase(){
        return battlePhase;
    }

}
