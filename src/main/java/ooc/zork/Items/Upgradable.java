package ooc.zork.Items;

import ooc.zork.Items.Misc.Rune;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teama on 1/24/2018.
 */

public abstract class Upgradable {

    int durability;

    List<Rune> slots = new ArrayList<Rune>();

    void addRunes(Rune r){
        slots.add(r);
    }

    void removeRunes(Rune r){
        slots.remove(r);
    }


}
