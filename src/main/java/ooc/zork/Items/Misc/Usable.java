package ooc.zork.Items.Misc;

import ooc.zork.Items.Item;

/**
 * Created by Teama on 1/28/2018.
 */
public abstract  class Usable implements Item {

    public abstract void apply();
}
