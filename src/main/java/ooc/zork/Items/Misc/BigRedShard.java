package ooc.zork.Items.Misc;

import ooc.zork.App;
import ooc.zork.Items.Item;

/**
 * Created by Teama on 1/28/2018.
 */
public class BigRedShard extends Usable {

    private final int SPACE = 1;
    private String name = "Big Red Shard";
    private int healing = 15;

    @Override
    public int spaceTaken() {
        return SPACE;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("A Big red shard, filling with life energy. An old lore once tell that it has an ability to fill your will, much better than the Red Shard");
    }

    @Override
    public void apply() {
        System.out.println("You use Big Red Shard. You gained 15 hp!");
        App.player.setCurrentHP(App.player.getCurrentHP()+healing);
    }
}
