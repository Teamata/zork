package ooc.zork.Items.Misc;

import ooc.zork.App;

/**
 * Created by Teama on 1/29/2018.
 */
public class FortunePinky extends Usable{


    private final int SPACE = 1;
    private String name = "Fortune Pinky";
    private int healing = 25;

    @Override
    public int spaceTaken() {
        return SPACE;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("A fortunate cookie that'll heal you");
    }

    @Override
    public void apply() {
        System.out.println("You use FortunePinky. You gained 25 hp!");
        App.player.setCurrentHP(App.player.getCurrentHP()+healing);
    }
}
