package ooc.zork.Items.Misc;

import ooc.zork.App;

/**
 * Created by Teama on 1/28/2018.
 */
public class RedShard extends Usable{

    private final int SPACE = 1;
    private String name = "Red Shard";
    private int healing = 10;

    @Override
    public int spaceTaken() {
        return SPACE;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("A red shard, filling with energy. An old lore once tell that it has an ability to fill your will");
    }

    @Override
    public void apply() {
        System.out.println("You use Red Shard. You gain 10 hp!");
        App.player.setCurrentHP(App.player.getCurrentHP()+healing);
    }
}
