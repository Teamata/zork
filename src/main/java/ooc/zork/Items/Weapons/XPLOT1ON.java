package ooc.zork.Items.Weapons;

/**
 * Created by Teama on 1/29/2018.
 */
public class XPLOT1ON extends Weapon {

    private final String name = "XPLOT1ON";

    public XPLOT1ON(){
        atk = 20;
        space = 2;
        speedpenalty = 0;
        criticalRate = 0.3;
        criticalDamage = 0;
    }

    @Override
    public int spaceTaken() {
        return space;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("An item owns by Pan, is used to cast a spell name DDOS which deals lethal damage");
        System.out.println("Still, you're not a mage so you can't use it");
    }

}
