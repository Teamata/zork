package ooc.zork.Items.Weapons;

import ooc.zork.Items.Item;

/**
 * Created by Teama on 1/28/2018.
 */
public class BareHand extends Weapon{

    private final String name = "Bare hand";

    public BareHand(){
        atk = 3;
        space = 0;
        speedpenalty = 0;
        criticalRate = 0;
        criticalDamage = 0;
    }

    @Override
    public int spaceTaken() {
        return space;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("The most powerful weapon from oneself, as one are in such desperate need of protection");
        System.out.println("It may not be the best, but at least you learn how to fight hopelessly");
    }
}
