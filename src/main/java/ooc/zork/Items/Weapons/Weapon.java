package ooc.zork.Items.Weapons;

import ooc.zork.Items.Item;
import ooc.zork.Items.Upgradable;

/**
 * Created by Teama on 1/24/2018.
 */
public abstract class Weapon extends Upgradable implements Item{

    int atk;
    int speedpenalty;
    double criticalRate;
    double criticalDamage;
    int space;
    boolean equip;

    public int getAtk() {
        return atk;
    }

    public double getCriticalRate() {
        return criticalRate;
    }

    public int getSpeedpenalty() {
        return speedpenalty;
    }

    public double getCriticalDamage() {
        return criticalDamage;
    }

    public boolean isEquip(){
        return equip;
    }

    public void setEquip(boolean e){
        equip = e;
    }

    public String printEquip(){
        if(isEquip()){
            return " [equipped]";
        }
        return "";
    }
}
