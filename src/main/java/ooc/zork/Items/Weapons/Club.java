package ooc.zork.Items.Weapons;

/**
 * Created by Teama on 1/28/2018.
 */
public class Club extends Weapon {

    private final String name = "Club";

    public Club(){
        atk = 10;
        space = 2;
        speedpenalty = 0;
        criticalRate = 0.1;
        criticalDamage = 0.25;
    }

    @Override
    public int spaceTaken() {
        return space;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public void getDescription() {
        System.out.println("A wooden club, a little bit clunky to carry but can deal lethal damage to an opponent");
    }
}
