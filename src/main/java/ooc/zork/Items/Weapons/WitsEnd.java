package ooc.zork.Items.Weapons;

import ooc.zork.Items.Item;
import ooc.zork.Items.Upgradable;

/**
 * Created by Teama on 1/29/2018.
 */
public class WitsEnd extends Weapon {

    private final String name = "Wit's end";

    public WitsEnd(){
        atk = 16;
        space = 2;
        speedpenalty = 1;
        criticalRate = 0.125;
        criticalDamage = 0.12;
    }

    @Override
    public int spaceTaken() {
        return space;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("A mysterious sword inhabit with dark essence");
    }
}
