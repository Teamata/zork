package ooc.zork.Items.Weapons;

/**
 * Created by Teama on 1/28/2018.
 */
public class BrokenSword extends Weapon{

    private final String name = "Broken Sword";

    public BrokenSword(){
        atk = 6;
        space = 1;
        speedpenalty = 0;
        criticalRate = 0.05;
        criticalDamage = 0;
    }

    @Override
    public int spaceTaken() {
        return space;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public void getDescription() {
        System.out.println("A broken sword, looks unusable better than nothing");
    }
}
