package ooc.zork.Items.Armors;

import ooc.zork.Items.Item;
import ooc.zork.Items.Upgradable;

/**
 * Created by Teama on 1/24/2018.
 */
public abstract class Armor extends Upgradable implements Item {

    int space;
    int defense;
    boolean equip;

    @Override
    public int spaceTaken() {
        return space;
    }

    public int getDefense() {
        return defense;
    }

    public boolean isEquip(){
        return equip;
    }

    public void setEquip(boolean e){
        equip = e;
    }

    public String printEquip(){
        if(isEquip()){
            return " [equipped]";
        }
        return "";
    }
}
