package ooc.zork.Items.Armors;

/**
 * Created by Teama on 1/28/2018.
 */
public class WoodenShield extends Armor {

    String name = "Wooden Shield";

    public WoodenShield(){
        defense = 5;
        space = 1;

    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void getDescription() {
        System.out.println("A plane wooden shield");
    }
}
