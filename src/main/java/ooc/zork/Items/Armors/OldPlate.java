package ooc.zork.Items.Armors;

/**
 * Created by Teama on 1/28/2018.
 */
public class OldPlate extends Armor {

    String name = "Old Plate";

    public OldPlate() {
        defense = 9;
        space = 1;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void getDescription() {
        System.out.println("An old plate, once wore by a proud warrior.");
    }
}