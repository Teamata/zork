package ooc.zork.Items.Armors;

/**
 * Created by Teama on 1/29/2018.
 */
public class Ply extends Armor {
    private final String name = "Ply";

    public Ply(){
        defense = 13;
        space = 2;
    }

    @Override
    public int spaceTaken() {
        return space;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void getDescription() {
        System.out.println("A body of an individual name Ply that can be use as a shield");
    }
}
