package ooc.zork.Items.Armors;

/**
 * Created by Teama on 1/29/2018.
 */
public class BrokenArmor extends Armor {
    String name = "Old Plate";

    public BrokenArmor() {
        defense = 2;
        space = 0;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void getDescription() {
        System.out.println("Torn armor");
    }
}
