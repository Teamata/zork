package ooc.zork.Items;

import ooc.zork.Items.Armors.OldPlate;
import ooc.zork.Items.Armors.Ply;
import ooc.zork.Items.Armors.WoodenShield;
import ooc.zork.Items.Misc.BigRedShard;
import ooc.zork.Items.Misc.FortunePinky;
import ooc.zork.Items.Misc.RedShard;
import ooc.zork.Items.Weapons.BrokenSword;
import ooc.zork.Items.Weapons.Club;
import ooc.zork.Items.Weapons.WitsEnd;
import ooc.zork.Items.Weapons.XPLOT1ON;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */
public class ItemsFactory {

    private static Random rand = new Random();

    //weapon,runes,armors


    private static final HashMap<Double, Class<? extends Item>> itemsList = new HashMap<Double, Class<? extends Item>>() {
        {
            put(0.45, RedShard.class);
            put(0.2, BigRedShard.class);
            put(0.1, FortunePinky.class);
        }
    };

    private static final HashMap<Double, Class<? extends Item>> weaponList = new HashMap<Double,Class<? extends Item>>(){
        {
            put(0.5, BrokenSword.class);
            put(0.25, Club.class);
            put(0.08, WitsEnd.class);
            put(0.005, XPLOT1ON.class);
        }
    };

    private static final  HashMap<Double, Class<? extends Item>> armorList = new HashMap<Double,Class<? extends Item>>(){
        {
            put(0.3, OldPlate.class);
            put(0.4, WoodenShield.class);
            put(0.45, Ply.class);
        }
    };

    private static final HashMap<Integer,HashMap<Double, Class<? extends Item>>> randomizedDrop = new HashMap<Integer,HashMap<Double, Class<? extends Item>>>(){
        {
            put(0,itemsList);
            put(1,weaponList);
            put(2,armorList);
        }
    };


    private static Item drop(HashMap<Double, Class<? extends Item>> l){
        Item item = null;
        Double randomizedDouble = rand.nextDouble();
        for(Double d : l.keySet()){
            if(d > randomizedDouble){
                try {
                    item = l.get(d).newInstance();
                    return item;
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return item;
    }

    public static Item getRandomizedItem(){
        HashMap<Double,Class<?extends Item>> random = randomizedDrop.get(rand.nextInt(randomizedDrop.size()));
        return drop(random);
    }

    public static Item getRandomizedWeapon(){
        return drop(weaponList);
    }

    public static Item getRandomizedArmor(){
        return drop(armorList);
    }

    public static Item getRandomizedMisc(){
        return drop(itemsList);
    }


    public static int getSize(){
        return itemsList.size();
    }
}
