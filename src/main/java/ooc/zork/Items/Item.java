package ooc.zork.Items;

/**
 * Created by Teama on 1/24/2018.
 */
public interface Item {

    int spaceTaken();
    String getName();
    void getDescription();


}
