package ooc.zork;

import ooc.zork.Entity.Combatable;

import java.util.Random;

/**
 * Created by Teama on 1/24/2018.
 */

public class Combat {
    //calculating atk
    final static Random rand = new Random();

    public static void fight(Combatable one, Combatable two){
        if(one.getSpeed() > two.getSpeed()){
            attack(one,two);
            if(!two.isDead()) {
                attack(two, one);
            }
        }
        else{
            attack(two,one);
            if(!one.isDead()) {
                attack(one, two);
            }
        }
    }

    //a attack b
    public static void attack(Combatable one, Combatable two){
        double chancestoCrit = rand.nextDouble();
        int damage =  one.getAttack()-two.getDefense();
        if(damage<0){
            damage = 0;
        }
        if(chancestoCrit < one.getCriticalRate()){
            damage = (int) (damage*one.getCriticalDamage());
            System.out.println(one.getName() + " deals " + damage + " to " + two.getName() + " .It's extremely optimized!");
        }
        else{
            System.out.println(one.getName() + " deals " + damage + " to " + two.getName());
        }
        int tempHP = two.getCurrentHP()-damage;
        if(tempHP<=0){
            tempHP = 0;
        }
        two.setCurrentHP(tempHP);
        if(two.getCurrentHP()==0){
            two.setDead();
            App.world.getCurrentLevel().addKilledMonster();
            System.out.println(two.getName() + " is dead!");
        }
    }


}
