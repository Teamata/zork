package ooc.zork.World;
import ooc.zork.App;
import ooc.zork.NPC.NPC;
import ooc.zork.Objectives.Objective;
import ooc.zork.Objectives.ObjectiveFactory;
import ooc.zork.World.Rooms.Room;
import ooc.zork.World.Rooms.RoomsFactory;

import java.util.*;

/**
 * Created by Teama on 1/24/2018.
 */

public class Level {

    private int level;
    private int npcCount = 0;
    private int monsterCount = 0;
    private Room[][] rooms;
    private boolean[][] hasRoom;
    private Objective objective;
    private int[] objectiveRoomLocation;
    private Room objectiveRoom;
    private Room startingRoom;
    private boolean hasPlayer = false;
    private int playerStartingPositionX;
    private int playerStartingPositionY;
    private boolean[][] visited;
    private boolean foundMap;
    private List<NPC> npcList = new ArrayList<>();
    private int monsterKilled = 0;
    final private Random rand = new Random();
    final private static Map<Integer, int[]> randomizedDirection = new HashMap<>();
    final private static Map<Integer, int[]> pathInBetween = new HashMap<>();


    //[row,column]
    static {
        randomizedDirection.put(0, new int[]{0, -2});//left
        randomizedDirection.put(1, new int[]{0, 2});//right
        randomizedDirection.put(2, new int[]{-2, 0});//top
        randomizedDirection.put(3, new int[]{2, 0});//bottom
        pathInBetween.put(0, new int[]{0, -1});//left
        pathInBetween.put(1, new int[]{0, 1});//right
        pathInBetween.put(2, new int[]{-1, 0});//top
        pathInBetween.put(3, new int[]{1, 0});//bottom
    }

    //calling this will automatically generate level.
    Level(int n,int m, int level) {
        setFoundMap(false);
        this.level = level;
        visited = new boolean[n][m];
        hasRoom = new boolean[n][m];
        rooms = new Room[n][m];
        int startingX = n / 2;
        int startingY = m / 2;
        playerStartingPositionX = startingX;
        playerStartingPositionY = startingY;
        generateLevel(startingX, startingY,level);
        addVisited();
    }


    public int getLevel() {
        return level;
    }

    boolean validGrid(int[] coordinate) {
        //check if out of bound
        return validGrid(coordinate[0], coordinate[1]);
    }

    public boolean validGrid(int coordinateX, int coordinateY) {
        //check if out of bound
        return !((coordinateX < 0 || coordinateX > rooms.length - 1) || (coordinateY < 0 || coordinateY > rooms.length - 1));
    }

    private void generateLevel(int currentX, int currentY, int level) {
        //
        LinkedList<int[]> frontiers = new LinkedList<>();
        frontiers.add(new int[]{currentX, currentY});
        while (!frontiers.isEmpty()) {
            int[] temp = frontiers.remove();
            if (!hasRoom[temp[0]][temp[1]]) {
                hasRoom[temp[0]][temp[1]] = true;
                generateRoom(temp[0],temp[1]);
                for (int r = 0; r < 4; r++) {
                    int randompos = rand.nextInt(4);
                    int newX = temp[0] + randomizedDirection.get(randompos)[0];
                    int newY = temp[1] + randomizedDirection.get(randompos)[1];
                    int[] possiblyNewRoom = new int[]{newX, newY};
                    if (validGrid(newX, newY) && !hasRoom[newX][newY]) {
                        int pathX = temp[0] + pathInBetween.get(randompos)[0];
                        int pathY = temp[1] + pathInBetween.get(randompos)[1];
                        if(!hasRoom[pathX][pathY]) {
                            generateRoom(pathX, pathY);
                            hasRoom[pathX][pathY] = true;
                        }
                        objectiveRoomLocation = new int[]{pathX, pathY};
                        frontiers.add(possiblyNewRoom);
//                        System.out.println(rooms[temp[0] + pathInBetween.get(randompos)[0]][temp[1] + pathInBetween.get(randompos)[1]].hasNPC());
                    }
                }
            }
        }
        rooms[currentX][currentY] = RoomsFactory.getStartRoom(level);
        startingRoom = rooms[currentX][currentY];
        rooms[objectiveRoomLocation[0]][objectiveRoomLocation[1]] = RoomsFactory.getObjectiveRoom(level);
        objectiveRoom = rooms[objectiveRoomLocation[0]][objectiveRoomLocation[1]];
        countTotalNPC();
        countTotalMonster();
        setQuest(level);
    }

    public void resetLevel(){
        for(int r=0;r<rooms.length;r++){
            for(int c=0;c<rooms.length;c++){
                if(hasRoom[r][c]){
                    rooms[r][c].setItem();
                    if(!rooms[r][c].isTheEndRoom() && rooms[r][c].getMonster()!=null) {
                        rooms[r][c].getMonster().setAlive();
                    }
                }
            }
        }
        setMonsterKilled(0);
    }

    public void displayLevel() {
        StringBuilder display = new StringBuilder();
        display.append("|");
        for(int x = 0; x < rooms.length; x++) {
            display.append("X");
        }
        display.append("|");
        display.append('\n');
        for (int r = 0; r < rooms.length; r++) {
            display.append("|");
            for (int c = 0; c < rooms.length; c++) {
                if (hasPlayer && App.player.getPositionX() == r && App.player.getPositionY() == c) {
                    display.append("O");
                } else if (!hasRoom[r][c] && (visited[r][c] || getFoundMap())) {
                    display.append("X");
                }else {
                    if(r == getStartingX() && c == getStartingY()){
                        display.append("+");
                    }
                    else if ((hasRoom[r][c]) && rooms[r][c].isTheEndRoom() && (visited[r][c] || getFoundMap())){
                        display.append("!");
                    }
                    else if (hasRoom[r][c] && rooms[r][c].hasNPC() && (visited[r][c] || getFoundMap() )){
                        display.append("?");
                    }
                    else {
                        if(getFoundMap() || visited[r][c]) {
                            display.append(" ");
                        }
                        else{
                            display.append("X");
                        }
                    }
                }
            }
            display.append("|");
            display.append('\n');
        }
        display.append("|");
        for(int x = 0; x < rooms.length; x++) {
            display.append("X");
        }
        display.append("|");
        display.append('\n');
        System.out.println(display.toString());
        if(getFoundMap()){
            System.out.println("Total Monster left :" + (getMonsterCount() - getMonsterKilled()));
            System.out.println("Monster that you've killed " + getMonsterKilled());
        }
    }

    private void generateRoom(int currentX, int currentY) {
        //Generate random room for that particular grid
        this.rooms[currentX][currentY] = RoomsFactory.getRoom();
    }

    private void countTotalNPC(){
        for(int a = 0; a<rooms.length;a++){
            for(int b=0; b<rooms.length;b++){
                if(hasRoom[a][b]){
                    if(rooms[a][b].hasNPC()) {
                        this.incrementNPCCount();
                    }
                }
            }
        }
    }

    private void setQuest(int level){
        objective = ObjectiveFactory.getObjective(objectiveRoom.hasNPC(),level);
    }

    //exclude boss monsters
    private void countTotalMonster(){
        for(int a = 0; a<rooms.length;a++){
            for(int b=0; b<rooms.length;b++){
                if(rooms[a][b]!=null && rooms[a][b].hasMonster() && !rooms[a][b].isTheEndRoom()){
                    monsterCount++;
                }
            }
        }
    }

    public void addVisited(){
        visited[App.player.getPositionX()][App.player.getPositionY()] = true;
        if(validGrid(App.player.getPositionX(),App.player.getPositionY()+1) && !hasRoom[App.player.getPositionX()][App.player.getPositionY()+1]){
            visited[App.player.getPositionX()][App.player.getPositionY()+1] = true;
        }
        if(validGrid(App.player.getPositionX(),App.player.getPositionY()-1) && !hasRoom[App.player.getPositionX()][App.player.getPositionY()-1]){
            visited[App.player.getPositionX()][App.player.getPositionY()-1] = true;
        }
        if(validGrid(App.player.getPositionX()+1,App.player.getPositionY()) && !hasRoom[App.player.getPositionX()+1][App.player.getPositionY()]){
            visited[App.player.getPositionX()+1][App.player.getPositionY()] = true;
        }
        if(validGrid(App.player.getPositionX()-1,App.player.getPositionY()) && !hasRoom[App.player.getPositionX()-1][App.player.getPositionY()]){
            visited[App.player.getPositionX()-1][App.player.getPositionY()] = true;
        }
    }

    public Room[][] getRooms() {
        return rooms;
    }

    public boolean[][] getHasRoom() {
        return hasRoom;
    }

    public boolean getHasPlayer(){
        return hasPlayer;
    }

    protected void setHasPlayer(boolean hasPlayer) {
        this.hasPlayer = hasPlayer;
    }

    public Room getCurrentRoom() {
        return rooms[App.player.getPositionX()][App.player.getPositionY()];
    }

    public void getObjective() {
        objective.getDescription();
    }

    public boolean isClear(){
        return objective.isCleared();
    }

    public int getNpccount() {
        return npcCount;
    }

    private void incrementNPCCount(){
        this.npcCount += 1;
    }

    public int getMonsterCount() {
        return monsterCount;
    }

    public int getMonsterKilled() {
        return monsterKilled;
    }

    private void setMonsterKilled(int num){
        this.monsterKilled = num;
    }

    public void addKilledMonster(){
        this.monsterKilled++;
    }

    public Room getObjectiveRoom() {
        return objectiveRoom;
    }

    public int getStartingX() {
        return playerStartingPositionX;
    }

    public int getStartingY() {
        return playerStartingPositionY;
    }

    public void setFoundMap(boolean foundMap) {
        this.foundMap = foundMap;
    }

    public boolean getFoundMap(){
        return foundMap;
    }

    public Room getStartingRoom() {
        return startingRoom;
    }

}

