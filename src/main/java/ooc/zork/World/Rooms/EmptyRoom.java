package ooc.zork.World.Rooms;
import ooc.zork.Entity.Combatable;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;
import ooc.zork.Items.ItemsFactory;
import ooc.zork.NPC.NPC;

import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */

public class EmptyRoom implements Room{

    //No monster, MAY HAVE item

    private Random rand = new Random();
    private boolean canLeave = true;
    private Item item;

    EmptyRoom(){
        item = ItemsFactory.getRandomizedItem();
    }


    @Override
    public void getDescription() {
        System.out.println("This room is filled with nothingness but a single torch on the wall");
        System.out.println("Even so you feel comfortable enough to let down your guard for a moment.");
    }

    @Override
    public boolean canLeave() {
        return !this.canLeave;
    }

    @Override
    public void removeItem() {
        item = null;
    }

    @Override
    public void lookAround() {
        System.out.println("You look around, and you've found out that");
        if(item==null) {
            System.out.println("This room doesn't contain anything useful for you");
        }
        else {
            System.out.println("There is " + item.getName() + " lying on the floor");
        }
    }


    @Override
    public void getObjective() {
        System.out.println("This room has no objective");
    }

    @Override
    public boolean isTheEndRoom() {
        return false;
    }

    @Override
    public boolean hasMonster() {
        return false;
    }

    @Override
    public boolean hasNPC() {
        return false;
    }

    @Override
    public boolean hasItem() {
        return item!=null;
    }

    @Override
    public void setCanLeave(boolean s) {
        this.canLeave = s;
    }

    @Override
    public void setItem() {
        item = ItemsFactory.getRandomizedItem();
    }

    @Override
    public NPC getNPC() {
        return null;
    }

    @Override
    public Monster getMonster() {
        return null;
    }

    @Override
    public Item getItem() {
        return item;
    }

}
