package ooc.zork.World.Rooms;

import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Entity.Monsters.MonstersFactory;
import ooc.zork.Items.Item;
import ooc.zork.Items.ItemsFactory;
import ooc.zork.NPC.NPC;
import ooc.zork.NPC.NPCsFactory;
import ooc.zork.Objectives.Objective;
import ooc.zork.Objectives.ObjectiveFactory;

import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */

//aka boss room
public class ObjectiveRoom implements Room {

    private Random rand = new Random();
    private boolean canLeave = true;
    private Item item = null;
    private NPC npc = null;
    private Monster monster = null;
    private boolean quest = new Random().nextBoolean();

    ObjectiveRoom(int level){
        if(quest){
            npc = NPCsFactory.getObjectiveNPC(level);
        }
        else{
            monster = MonstersFactory.getBossMonster(level);
        }
        item = ItemsFactory.getRandomizedItem();
    }


    @Override
    public void getDescription() {
        if(quest) {
            System.out.println(npc.getName() + " is standing, he cracks a little smile as he notices your appearance");
            npc.greeting();
        }
        else{
            System.out.println("As soon as you enter the room, you sense a fixed glare. The atmosphere is filled with an overwhelming aura");
            System.out.println(monster.getName() + " seems like it is ready to engage in battle if you approach it");
        }
    }

    @Override
    public boolean canLeave() {
        return canLeave;
    }

    @Override
    public void removeItem() {
        item = null;
    }

    @Override
    public void lookAround() {
        if(quest){
            System.out.println(npc.getName() + " is in this room, he does not seems to be interested in going anywhere else");
        }
        else{
            if(monster.isDead()){
                System.out.println("There is no monster here");
            }
            else{
                System.out.println(monster.getName() + " seems like it is ready to engage in battle if you approach it");
            }
        }
        if(item==null) {
            System.out.println("This room doesn't contain anything useful for you");
        }
        else {
            System.out.println("There is " + item.getName() + " in this room");
        }
    }

    @Override
    public void getObjective() {
        System.out.println("This is the objective room of this floor");
    }

    @Override
    public boolean isTheEndRoom() {
        return true;
    }

    @Override
    public boolean hasMonster() {
        return monster != null && !monster.isDead();
    }


    @Override
    public boolean hasNPC() {
        return npc != null;
    }

    @Override
    public boolean hasItem() {
        return item!=null;
    }

    @Override
    public void setCanLeave(boolean s) {
        this.canLeave = s;
    }

    @Override
    public NPC getNPC() {
        return npc;
    }

    @Override
    public Monster getMonster() {
        return monster;
    }

    @Override
    public Item getItem() {
        return item;
    }

    @Override
    public void setItem() {
    }
}
