package ooc.zork.World.Rooms;

import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Entity.Monsters.MonstersFactory;
import ooc.zork.Items.Item;
import ooc.zork.Items.ItemsFactory;
import ooc.zork.NPC.NPC;
import ooc.zork.World.World;

import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */

public class MonsterRoom implements Room{

    private Random rand = new Random();
    private boolean canLeave = rand.nextBoolean();
    private Item item;
    private Monster monster;

    MonsterRoom(){
        item = ItemsFactory.getRandomizedItem();
        monster = MonstersFactory.getRandomizedMonster(World.getCreatingLevel());
    }

    @Override
    public void getDescription() {
        if(!monster.isDead()) {
            System.out.println("You can feel the air thicken by the presence of an unknown entity that is in this room");
        }
        else{
            System.out.println("There is nothing but a worthless body of a monster lying on the floor");
        }
        if(!canLeave) {
            System.out.println("That unknown entity is blocking the way out");
        }
    }

    private void checkMonster(){
        if(monster.isDead()){
            System.out.println(monster.getName() + " [Dead] ");
        }
        else{
            System.out.println("There is " + monster.getName() + " lurking around in this room");
        }
    }

    public void setCanLeave(boolean canLeave) {
        this.canLeave = canLeave;
    }

    @Override
    public boolean canLeave() {
        return this.canLeave;
    }

    @Override
    public void removeItem() {
        item = null;
    }

    @Override
    public void lookAround() {
        System.out.println("You look around, and you've found out that");
        checkMonster();
        if(item==null) {
            System.out.println("This room doesn't contain anything useful for you");
        }
        else {
            System.out.println("There is " + item.getName() + " in this room");
        }
    }

    @Override
    public void getObjective() {
        if(!canLeave) {
            System.out.println("You must eliminate an entity that's blocking your path");
        }
        else{
            System.out.println("This room has no objective");
        }
    }

    @Override
    public boolean isTheEndRoom() {
        return false;
    }

    @Override
    public boolean hasMonster() {
        return !monster.isDead();
    }

    @Override
    public boolean hasNPC() {
        return false;
    }

    @Override
    public boolean hasItem() {
        return item!=null;
    }

    @Override
    public NPC getNPC() {
        return null;
    }

    @Override
    public Monster getMonster() {
        return monster;
    }

    @Override
    public Item getItem() {
        return item;
    }

    @Override
    public void setItem() {
        item = ItemsFactory.getRandomizedItem();
    }
}
