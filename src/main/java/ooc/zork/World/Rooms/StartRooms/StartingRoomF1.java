package ooc.zork.World.Rooms.StartRooms;

import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;
import ooc.zork.App;
import ooc.zork.Items.Misc.RedShard;
import ooc.zork.NPC.NPC;
import ooc.zork.World.Rooms.Room;

import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */
public class StartingRoomF1 implements Room {

    private Random rand = new Random();
    final boolean objective = true;
    private Item item = null;
    private NPC npc = null;
    private Monster monster = null;
    private boolean canLeave = true;

    public StartingRoomF1(){
        item = new RedShard();
    }

    @Override
    public void getDescription() {
        System.out.println("As a fellow adventurer, one should not miss an opportunity to explore such mysterious place");
        System.out.println("Despite rumors of extreme danger that could cost one's life. You, " + App.player.getName() + " decides to step your foot in");
        System.out.println("There's no turning back now. Good luck");
    }

    @Override
    public boolean canLeave() {
        return canLeave;
    }

    @Override
    public void removeItem() {
        item = null;
    }

    @Override
    public void lookAround() {
        System.out.println("This is where you first begins your journey. There is a small bonfire that you can rest");
        if(item==null) {
            System.out.println("This room doesn't contain anything useful for you");
        }
        else {
            System.out.println("There is " + item.getName() + " in this room");
        }
    }

    @Override
    public void getObjective() {
        System.out.println("There is no objective here");
    }

    @Override
    public boolean isTheEndRoom() {
        return false;
    }

    @Override
    public boolean hasMonster() {
        return false;
    }

    @Override
    public boolean hasNPC() {
        return false;
    }

    @Override
    public boolean hasItem() {
        return item!=null;
    }

    @Override
    public void setCanLeave(boolean s) {
        this.canLeave = s;
    }

    @Override
    public void setItem() {
        item = new RedShard();
    }

    @Override
    public NPC getNPC() {
        return npc;
    }

    @Override
    public Monster getMonster() {
        return monster;
    }

    @Override
    public Item getItem() {
        return item;
    }
}
