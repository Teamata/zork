package ooc.zork.World.Rooms;
import ooc.zork.World.Rooms.StartRooms.StartingRoomF1;
import ooc.zork.World.Rooms.StartRooms.StartingRoomF2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

/**
 * Created by Teama on 1/25/2018.
 */
public class RoomsFactory {

    private static Random rand = new Random();

    private static List<Double> roomsProbability = new ArrayList<>();
    static {
        roomsProbability.add(0.3);
        roomsProbability.add(0.9);
        roomsProbability.add(1.0);
    }

    private static final HashMap<Integer, Class<? extends Room>> rooms = new HashMap<Integer, Class <? extends Room>>() {
        {
            // rooms are added here using lambdas. It is also possible to dynamically add commands without editing the code.
            put(0,EmptyRoom.class);
            put(1,MonsterRoom.class);
            put(2,NPCRoom.class);
        }
    };

    private static final HashMap<Integer, Class<? extends Room>> startingRooms = new HashMap<Integer, Class <? extends Room>>() {
        {
            // rooms are added here using lambdas. It is also possible to dynamically add commands without editing the code.
            put(1, StartingRoomF1.class);
            put(2, StartingRoomF2.class);
        }
    };


    public static Room getRoom() {
        Double r = rand.nextDouble();
        Room room = null;
        for (int i = 0; i < roomsProbability.size(); i++) {
            if (r < roomsProbability.get(i)) {
                Class<? extends Room> roomType = rooms.get(i);
                try {
                    room = roomType.newInstance();
                    return room;
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return room;
    }

    public static Room getStartRoom(Integer num){
        Class<? extends Room> roomType;
        //floor 2nd and above.
        if(num > 2){
            roomType = startingRooms.get(2);
        }
        else{
            roomType = startingRooms.get(num);
        }
        Room startRoom = null;
        try {
            startRoom = roomType.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return startRoom;
    }

    public static int getRoomSize(){return rooms.size();}

    public static Room getObjectiveRoom(int level){
        return new ObjectiveRoom(level);
    }

}
