package ooc.zork.World.Rooms;

import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;
import ooc.zork.NPC.NPC;

/**
 * Created by Teama on 1/25/2018.
 */
public interface Room {

    //text describe characteristic of the room
    void getDescription();
    //has objective to get pass the room or not
    boolean canLeave();

    void removeItem();

    //get infomation about this room such as monster,item,npc
    void lookAround();

    //Some room you may need to complete certain task to pass
    void getObjective();

    boolean isTheEndRoom();

    boolean hasMonster();

    boolean hasNPC();

    boolean hasItem();

    void setCanLeave(boolean s);

    void setItem();

    NPC getNPC();

    Monster getMonster();

    Item getItem();


}
