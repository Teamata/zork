package ooc.zork.World.Rooms;

import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;
import ooc.zork.Items.ItemsFactory;
import ooc.zork.NPC.NPC;
import ooc.zork.NPC.NPCsFactory;
import ooc.zork.World.Level;
import ooc.zork.World.World;

import java.util.Random;

/**
 * Created by Teama on 1/27/2018.
 */
public class NPCRoom implements Room {

    private Random rand = new Random();
    private Item item;
    private NPC npc;
    private boolean containNPC;
    private boolean canLeave;

    NPCRoom(){
        item = ItemsFactory.getRandomizedItem();
        npc = NPCsFactory.getNPC(World.getCreatingLevel());
        if(this.npc!=null){
            this.containNPC = true;
        }
        canLeave = true;
    }

    @Override
    public void getDescription() {
        if(npc!=null) {
            npc.greeting();
        }
        else {
            System.out.println("Looks like there's a trace of someone that's used to be here");
        }
    }

    @Override
    public boolean canLeave() {
        return !this.canLeave;
    }

    @Override
    public void removeItem() {
        item = null;
    }

    @Override
    public void lookAround() {
        System.out.println("You look around, and you've found out that");
        if(npc==null){
            System.out.println("No one is here but something doesn't feel quite right");
        }
        else{
            System.out.println(npc.getName() + " is in this room, he does not seems to be interested in going anywhere else");
        }
        if(item==null) {
            System.out.println("This room doesn't contain anything useful for you");
        }
        else {
            System.out.println("There is " + item.getName() + " in this room");
        }
    }

    @Override
    public void getObjective() {
        System.out.println("This room has no objective");
    }

    @Override
    public boolean isTheEndRoom() {
        return false;
    }

    @Override
    public boolean hasMonster() {
        return false;
    }

    @Override
    public boolean hasNPC() {
        return this.containNPC;
    }

    @Override
    public boolean hasItem() {
        return item!=null;
    }

    @Override
    public void setCanLeave(boolean s) {
        this.canLeave = s;
    }

    @Override
    public NPC getNPC() {
        return npc;
    }

    @Override
    public Monster getMonster() {
        return null;
    }

    @Override
    public Item getItem() {
        return item;
    }

    @Override
    public void setItem() {
        item = ItemsFactory.getRandomizedItem();
    }
}
