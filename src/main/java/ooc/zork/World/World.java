package ooc.zork.World;

import ooc.zork.App;

import java.util.HashMap;
import java.util.Random;

/**
 * Created by Teama on 1/24/2018.
 */
public class World {

    //keep track of the game, stats, levels and rooms.

    public static HashMap<Integer,Level> allLevels = new HashMap<Integer, Level>();
    private Level currentLevel;
    private static int creatingLevel;
    int monsterKilled;
    final Random rand = new Random();

    //initialized the world
    public void setUpWorld(){
        int totalLevels = new Random().nextInt(1)+3;
        System.out.println("Total level : " + totalLevels);
        int randomGridSize = rand.nextInt(5) + 5;
        int startingX = randomGridSize/2;
        int startingY = randomGridSize/2;
        App.player.setPositionX(startingX);
        App.player.setPositionY(startingY);
        for(int r = 1; r < totalLevels+1;r++){
            creatingLevel = r;
            Level l = new Level(randomGridSize,randomGridSize,r);
            allLevels.put(r,l);
            randomGridSize = rand.nextInt(2) + 5;
        }
        currentLevel = allLevels.get(1);
        currentLevel.setHasPlayer(true);
        currentLevel.setFoundMap(true);
    }

    public void setCurrentLevel(Level currentLevel) {
        this.currentLevel = currentLevel;
    }

    public boolean nextLevel(){
        if(allLevels.containsKey(currentLevel.getLevel()+1)){
            this.currentLevel.setHasPlayer(false);
            this.currentLevel = allLevels.get(currentLevel.getLevel()+1);
            this.currentLevel.setHasPlayer(true);
            return true;
        }
        return false;
    }

    public Level getCurrentLevel() {
        return currentLevel;
    }

    public Level getLevel(int l){
        return allLevels.get(l);
    }

    public int getMonsterKilled() {
        return monsterKilled;
    }

    public static int getCreatingLevel() {
        return creatingLevel;
    }
}
