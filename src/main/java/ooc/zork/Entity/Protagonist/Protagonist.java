package ooc.zork.Entity.Protagonist;

import ooc.zork.Entity.Combatable;
import ooc.zork.Items.Armors.Armor;
import ooc.zork.Items.Armors.BrokenArmor;
import ooc.zork.Items.Item;
import ooc.zork.Items.Weapons.BareHand;
import ooc.zork.Items.Weapons.Weapon;

import java.util.Random;

public class Protagonist extends Combatable {

    Inventory bag;
    private Weapon barehand = new BareHand();
    private Weapon mainHand;
    private Armor brokenArmor = new BrokenArmor();
    private Armor armor;
    private int monsterKilled;
    private int totalAttack;
    private int totalDefense;
    private double totalCriticalRate;
    private double totalCriticalDamage;
    private int totalSpeed;
    private int positionX;
    private int positionY;
    private int currentStack; //level :'D
    private int bottomStack;
    private int topStack;
    private Random rand = new Random();

    public Protagonist(){
        mainHand = barehand;
        armor = brokenArmor;
        currentStack = 1;
        level = currentStack;
        bottomStack = 0;
        topStack = 30;
        attack = 10;
        defense = 5;
        criticalRate = 0.20;
        criticalDamage = 1.20;
        speed = 3;
        maxHP = 20;
        currentHP = 20;
        totalAttack = attack + mainHand.getAtk();
        totalDefense = defense + armor.getDefense();
        totalCriticalRate = criticalRate + mainHand.getCriticalRate();
        totalCriticalDamage = criticalDamage + mainHand.getCriticalDamage();
        totalSpeed = speed + mainHand.getSpeedpenalty();
        isAlive = true;
        bag = new Inventory(10);
    }

    @Override
    public void printStats(){
        StringBuilder st = new StringBuilder();
        st.append("Character name " + this.getName() +"\n"+
                " Level " + this.getLevel()+"\n"+
                " EXP: " + this.bottomStack +"/"+this.topStack+"\n"+
                " HP " + getCurrentHP()+"/"+getMaxHP() +"\n"+
                " Attack " + this.getAttack() +"\n"+
                " Defense " + this.getDefense() +"\n"+
                " Critical rate " + this.getCriticalRate() + "\n"+
                " Critical damage " + this.getCriticalDamage() + "\n"+
                " Speed " + this.getSpeed()+ "\n"+
                " Bag " + bag.getCurrentSize()+"/"+bag.getSize() + "\n"+
                " Monster killed " + monsterKilled
        );
        System.out.println(st);
    }

    private void stackOverflow(){
        int statGain = rand.nextInt(4)+2;
        System.out.println("You gain " +statGain + " attack!");
        attack += statGain;
        statGain = rand.nextInt(4)+2;
        System.out.println("You gain " +statGain + " defense!");
        defense += statGain;
        statGain = rand.nextInt(2)+1;
        System.out.println("You gain " +statGain + " speed!");
        speed += statGain;
        statGain = rand.nextInt(10)+5;
        maxHP += statGain;
        System.out.println("You gain " + statGain + " more HP!. You now have a total of " + getMaxHP() + " HP");
        currentHP += statGain;
    }

    public void gainEXP(int exp){
        monsterKilled++;
        System.out.println("You've gained " + exp + " exp!");
        bottomStack += exp;
        while(bottomStack>=topStack){
            System.out.println("StackOverflow! you leveled up!");
            currentStack++;
            stackOverflow();
            System.out.println("Current lv. : " + currentStack);
            topStack *= 1.6;
        }
    }

    public void equip(int index){
        if(bag.getCurrentSize() <= index-1 || index-1 < 0){
            System.out.println("That's not the right index!");
        }
        else {
            Item i = bag.getItem(index-1);
            if (!Weapon.class.isAssignableFrom(i.getClass()) && !Armor.class.isAssignableFrom(i.getClass())) {
                System.out.println("this item can't be equip!");
            } else {
                if (Weapon.class.isAssignableFrom(i.getClass())) {
                    if (!mainHand.equals(barehand)) {
                        unequip(bag.indexOf(mainHand)+1);
                    }
                    System.out.println(this.getName() + " successfully equip " + i.getName());
                    mainHand = (Weapon) i;
                    mainHand.setEquip(true);
                    setTotalAttack((((Weapon) i).getAtk()));
                    setTotalCriticalDamage((((Weapon) i).getCriticalDamage()));
                    setTotalCriticalRate((((Weapon) i).getCriticalRate()));
                    setTotalSpeed((((Weapon) i).getSpeedpenalty()));
                    System.out.println("Your now have " + this.getAttack() + " attack!");
                } else {
                    if (!armor.equals(brokenArmor)) {
                        unequip(bag.indexOf(armor)+1);
                    }
                    System.out.println(this.getName() + " successfully equip " + i.getName());
                    armor = (Armor) i;
                    armor.setEquip(true);
                    setTotalDefense(((Armor) i).getDefense());
                    System.out.println("Your now have " + this.getDefense() + " defense!");
                }
            }
        }
    }

    public void unequip(int index){
        if(bag.getCurrentSize() <= index-1 || index-1 < 0){
            System.out.println("That's not the right index!");
        }
        else {
            Item i = bag.getItem(index-1);
            if (!Weapon.class.isAssignableFrom(i.getClass()) && !Armor.class.isAssignableFrom(i.getClass())) {
                System.out.println("That is not the right item!");
            } else {
                if (Weapon.class.isAssignableFrom(i.getClass())) {
                    ((Weapon) i).setEquip(false);
                    setTotalAttack(-1 * (((Weapon) i).getAtk()));
                    setTotalCriticalDamage(-1 * (((Weapon) i).getCriticalDamage()));
                    setTotalCriticalRate(-1 * (((Weapon) i).getCriticalRate()));
                    setTotalSpeed(-1 * (((Weapon) i).getSpeedpenalty()));
                    mainHand = barehand;
                } else {
                    ((Armor) i).setEquip(false);
                    setTotalDefense(-1 * (((Armor) i).getDefense()));
                    armor = brokenArmor;
                }
                System.out.println(this.getName() + " successfully unequip " + i.getName());
            }
        }
    }

    public boolean pickUp(Item i){
        return bag.addItems(i);
    }

    public boolean dropItem(int i){
        if(bag.getCurrentSize() <= i-1 || i-1 < 0){
            System.out.println("That's not the right index!");
            return false;
        }
        if((Weapon.class.isAssignableFrom(bag.getItem(i-1).getClass())) || (Armor.class.isAssignableFrom(bag.getItem(i-1).getClass()))){
            if(armor.equals(bag.getItem(i-1)) || mainHand.equals(bag.getItem(i-1))) {
                unequip(i);
            }
        }
        bag.dropItems(i-1);
        return true;
    }

    public boolean useItem(int i){
        if(bag.getCurrentSize() <= i-1 || i-1 < 0){
            System.out.println("That's not the right index!");
            return false;
        }
        return bag.useItem(i-1);
    }

    @Override
    public int getDefense(){
        return totalDefense;
    }

    @Override
    public double getCriticalDamage(){
        return totalCriticalDamage;
    }

    @Override
    public double getCriticalRate(){
        return totalCriticalRate;
    }

    @Override
    public int getAttack(){
        return totalAttack;
    }

    @Override
    public int getSpeed(){
        return totalSpeed;
    }

    @Override
    public int getLevel(){
        return getCurrentStack();
    }

    public int getCurrentStack() {
        return currentStack;
    }

    public int getPositionX() {
        return positionX;
    }

    public int getPositionY() {
        return positionY;
    }

    public void setPositionX(int positionX) {
        this.positionX = positionX;
    }

    public void setPositionY(int positionY) {
        this.positionY = positionY;
    }

    public Inventory getBag() {
        return bag;
    }

    public void setTotalAttack(int totalAttack) {
        this.totalAttack += totalAttack;
    }

    public void setTotalCriticalDamage(double totalCriticalDamage) {
        this.totalCriticalDamage += totalCriticalDamage;
    }

    public void setTotalCriticalRate(double totalCriticalRate) {
        this.totalCriticalRate += totalCriticalRate;
    }

    public void setTotalDefense(int totalDefense) {
        this.totalDefense += totalDefense;
    }

    public void setTotalSpeed(int totalSpeed) {
        this.totalSpeed += totalSpeed;
    }

}
