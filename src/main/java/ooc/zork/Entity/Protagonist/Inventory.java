package ooc.zork.Entity.Protagonist;

import ooc.zork.Items.Armors.Armor;
import ooc.zork.Items.Item;
import ooc.zork.Items.Misc.Usable;
import ooc.zork.Items.Weapons.Weapon;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teama on 1/24/2018.
 */
public class Inventory {

    int size;
    int currentSize;
    List<Item> stuffs;

    public Inventory(int s){
        size = s;
        currentSize = 0;
        stuffs = new ArrayList<Item>();
    }

    public List<Item> getStuffs() {
        return stuffs;
    }

    public int getSize() {
        return size;
    }
    public int getCurrentSize() {
        return currentSize;
    }

    public boolean contains(Item i){
        return stuffs.contains(i);
    }

    public int indexOf(Item i){
        return stuffs.indexOf(i);
    }
    public boolean isFull(){
        return currentSize > size;
    }

    public void printInventory(){
        StringBuilder print = new StringBuilder();
        print.append("List of items : You can only equip 1 Weapon / 1 Armor at a time \n");
        print.append("Currentsize : " + getCurrentSize()+"/"+getSize() + '\n');
        if(this.getCurrentSize()<=0){
            print.append("Sadly, Your bag currently contains nothing  \n");
        }
        else {
            for (int a = 0; a < stuffs.size(); a++) {
                print.append(a + 1).append(") ").append(stuffs.get(a).getName());
                if (Weapon.class.isAssignableFrom(stuffs.get(a).getClass())) {
                    print.append(((Weapon) stuffs.get(a)).printEquip());
                } else if (Armor.class.isAssignableFrom(stuffs.get(a).getClass())) {
                    print.append(((Armor) stuffs.get(a)).printEquip());
                }
                print.append('\n');
            }
        }
        System.out.println(print.toString());
    }

    public Item getItem(int i){
        return stuffs.get(i);
    }

    public boolean addItems(Item i){
        int temp = currentSize + i.spaceTaken();
        if(temp > size || this.isFull()) {
            System.out.println("You bag is full!");
            return false;
        }
        stuffs.add(i);
        currentSize += i.spaceTaken();
        System.out.format("%s has been added to your inventory \n",i.getName());
        return true;
    }

    public boolean dropItems(int i){
        if(i < getCurrentSize()) {
            Item item = stuffs.get(i);
            currentSize -= item.spaceTaken();
            stuffs.remove(i);
            return true;
        }
        return false;
    }

    public boolean useItem(int i){
        if(i < getCurrentSize()){
            Usable item = (Usable) stuffs.get(i);
            currentSize -= item.spaceTaken();
            item.apply();
            getStuffs().remove(item);
            return true;
        }
        return false;
    }
}
