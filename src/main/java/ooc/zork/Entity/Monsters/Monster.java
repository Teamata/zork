package ooc.zork.Entity.Monsters;

import ooc.zork.Entity.Combatable;
import ooc.zork.Items.Item;
import ooc.zork.Items.ItemsFactory;

/**
 * Created by Teama on 1/26/2018.
 */
public abstract class Monster extends Combatable {

    protected int exp;
    Item item;

    public Item dropItem(){
        return ItemsFactory.getRandomizedItem();
    }

    public int getExp(){
        return exp;
    }

    public abstract boolean isBossMonster();
}
