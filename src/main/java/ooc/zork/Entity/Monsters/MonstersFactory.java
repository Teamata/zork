package ooc.zork.Entity.Monsters;

import ooc.zork.Entity.Monsters.BossMonster.FooBar;
import ooc.zork.Entity.Monsters.BossMonster.TimeOut;
import ooc.zork.Entity.Monsters.NoobMonster.Memleak;
import ooc.zork.Entity.Monsters.BossMonster.NullPointer;
import ooc.zork.Entity.Monsters.BossMonster.OutofBound;
import ooc.zork.Entity.Monsters.NoobMonster.*;
import ooc.zork.Entity.Monsters.NoobMonster.Runtime;

import java.util.*;

public class MonstersFactory {

    final static Random rand = new Random();

    //separate into lv monster first//

    private static Map<Integer, Class<? extends Monster>> noobMonstersFirstFloor
            = new HashMap<Integer, Class<? extends Monster>>() {
        {
            put(0, Foo.class);
            put(1, Bar.class);
            put(2, Memleak.class);
        }
    };

    private static List<Double> firstFloorProbability = new ArrayList<>();
    static {
        firstFloorProbability.add(1.0);
        firstFloorProbability.add(0.6);
        firstFloorProbability.add(0.1);
    }

    private static Map<Integer, Class<? extends Monster>> noobMonstersSecondFloor
            = new HashMap<Integer, Class<? extends Monster>>() {
        {
            put(0, Foo.class);
            put(1, Bar.class);
            put(2, Memleak.class);
        }
    };

    public static List<Double> secondFloorProbability = new ArrayList<>();
    static {
        secondFloorProbability.add(0.3);
        secondFloorProbability.add(0.8);
        secondFloorProbability.add(1.0);
    }

    private static Map<Integer, Class<? extends Monster>> noobMonstersThirdFloor
            = new HashMap<Integer, Class<? extends Monster>>() {
        {
            put(0, Foo.class);
            put(1, Bar.class);
            put(2, Memleak.class);
            put(3, Runtime.class);
        }
    };

    public static List<Double> thirdFloorProbability = new ArrayList<>();
    static {
        thirdFloorProbability.add(0.1);
        thirdFloorProbability.add(0.3);
        thirdFloorProbability.add(0.7);
        thirdFloorProbability.add(1.0);
    }

    private static Map<Integer, Class<? extends Monster>> noobMonstersFourthFloor
            = new HashMap<Integer, Class<? extends Monster>>() {
        {
            put(0, Foo.class);
            put(1, Bar.class);
            put(2, Memleak.class);
            put(3, Runtime.class);
            put(4, InputTypeMismatch.class);

        }
    };

    public static List<Double> fourthFloorProbability = new ArrayList<>();
    static {
        fourthFloorProbability.add(0.1);
        fourthFloorProbability.add(0.2);
        fourthFloorProbability.add(0.6);
        fourthFloorProbability.add(0.9);
        fourthFloorProbability.add(1.0);
    }

    private static Map<Integer, Class<? extends Monster>> registeredBossMonsterClass
                = new HashMap<Integer, Class<? extends Monster>>() {
        {
            put(1, FooBar.class);
            put(2, TimeOut.class);
            put(3, NullPointer.class);
            put(4, OutofBound.class);
        }
    };

    private static Map<Integer, List<Double>> noobMonstersProbability
            = new HashMap<Integer, List<Double>>() {
        {
            put(1, firstFloorProbability);
            put(2, secondFloorProbability);
            put(3, thirdFloorProbability);
            put(4, fourthFloorProbability);
        }
    };

    private static Map<Integer, Map<Integer, Class<? extends Monster>>> randomizedAllNoobMonsters
            = new HashMap<Integer, Map<Integer, Class<? extends Monster>>>() {
        {
            put(1, noobMonstersFirstFloor);
            put(2, noobMonstersSecondFloor);
            put(3, noobMonstersThirdFloor);
            put(4, noobMonstersFourthFloor);
        }
    };

    public static Monster getRandomizedMonster(int level){
        Map<Integer,Class<?extends Monster>> random = randomizedAllNoobMonsters.get(level);
        List<Double> probability = noobMonstersProbability.get(level);
        Double r = rand.nextDouble();
        Monster mon = null;
        for (int i = 0; i < random.size(); i++) {
            if (r <probability.get(i)) {
                Class<? extends Monster> monsterType = random.get(i);
                try {
                    mon = monsterType.newInstance();
                    return mon;
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
        return mon;
    }

    //specified noob monster or boss monster
//    public static Monster getRandomMonster(int specifiedType) {
//        int monsterType = specifiedType;
//        if(specifiedType == -1) {
//            monsterType = rand.nextInt(monsters.size());
//        }
//        MapCommand<Integer, Class<? extends Monster>> setOfMonsters =  monsters.get(monsterType);
//        Class<? extends Monster> monsterClass = setOfMonsters.get(rand.nextInt(setOfMonsters.size()));
//        Monster mons = null;
//        try {
//            mons = monsterClass.newInstance();
//        } catch (InstantiationException | IllegalAccessException ex) {
//        } catch (NullPointerException npe) {
//            System.err.println("Error, monster not found");
//        }
//        return mons;
//    }

    public static Monster getBossMonster(int level){
        Class<? extends Monster> bossMonsterClass = registeredBossMonsterClass.get(level);
        Monster boss = null;
        try {
            boss = bossMonsterClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return boss;
    }

    public static int getNoobMonsterSize(){
        return noobMonstersFirstFloor.size();
    }

    public static int getBossMonsterSize(){
        return registeredBossMonsterClass.size();
    }

}
