package ooc.zork.Entity.Monsters.BossMonster;

import ooc.zork.Entity.Monsters.Monster;

/**
 * Created by Teama on 1/29/2018.
 */
public class OutofBound extends Monster {

    public OutofBound() {
        name = "OutofBound";
        attack = 50;
        defense = 25;
        speed = 12;
        level = 25;
        maxHP = 300;
        currentHP = 300;
        criticalRate = 0.4;
        criticalDamage = 1.5;
        isAlive = true;
        exp = 5000;
    }

    @Override
    public boolean isBossMonster() {
        return true;
    }
}
