package ooc.zork.Entity.Monsters.BossMonster;

import ooc.zork.Entity.Monsters.Monster;

/**
 * Created by Teama on 1/29/2018.
 */
public class TimeOut extends Monster {

    public TimeOut() {
        name = "TimeOut";
        attack = 28;
        defense = 20;
        speed = 20;
        level = 15;
        maxHP = 90;
        currentHP = 90;
        criticalRate = 0.4;
        criticalDamage = 1.2;
        isAlive = true;
        exp = 500;
    }

    @Override
    public boolean isBossMonster() {
        return true;
    }
}