package ooc.zork.Entity.Monsters.BossMonster;

import ooc.zork.Entity.Combatable;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;

/**
 * Created by Teama on 1/26/2018.
 */

public class FooBar extends Monster{

    public FooBar(){
        name = "FooBar";
        attack = 20;
        defense = 15;
        speed = 2;
        level = 9;
        maxHP = 70;
        currentHP = 70;
        criticalRate = 0.32;
        criticalDamage = 1.3;
        isAlive = true;
        exp = 150;
    }

    @Override
    public boolean isBossMonster() {
        return true;
    }
}
