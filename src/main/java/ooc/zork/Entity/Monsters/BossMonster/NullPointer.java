package ooc.zork.Entity.Monsters.BossMonster;

import ooc.zork.Entity.Monsters.Monster;

/**
 * Created by Teama on 1/29/2018.
 */
public class NullPointer extends Monster {

    public NullPointer(){
        name = "NullPointer";
        attack = 40;
        defense = 20;
        speed = 2;
        level = 20;
        maxHP = 190;
        currentHP = 190;
        criticalRate = 0.1;
        criticalDamage = 1.5;
        isAlive = true;
        exp = 2000;
    }

    @Override
    public boolean isBossMonster() {
        return true;
    }
}
