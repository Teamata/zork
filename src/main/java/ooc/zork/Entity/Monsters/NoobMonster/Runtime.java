package ooc.zork.Entity.Monsters.NoobMonster;

import ooc.zork.Entity.Monsters.Monster;

import java.util.Random;

/**
 * Created by Teama on 1/29/2018.
 */
public class Runtime extends Monster {

    Random rand = new Random();

    public Runtime(){
        attack = rand.nextInt(2) + 40;
        defense = rand.nextInt(2) + 20;
        maxHP = rand.nextInt(3) + 100;
        currentHP = maxHP;
        level = 25;
        speed = 16;
        criticalRate = 0.25;
        criticalDamage = 1.1;
        isAlive = true;
        name = "Runetime";
        exp = rand.nextInt(2) + 60;
    }

    @Override
    public boolean isBossMonster() {
        return false;
    }
}
