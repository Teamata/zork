package ooc.zork.Entity.Monsters.NoobMonster;

import ooc.zork.Entity.Combatable;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;

import java.util.Random;


public class Bar extends Monster{

    Random rand = new Random();

    public Bar(){
        name = "Bar";
        attack = rand.nextInt(3)+10;
        defense = rand.nextInt(2)+2;
        maxHP = rand.nextInt(5)+10;
        currentHP = maxHP;
        level = 2;
        speed = 0;
        criticalRate = 0.25;
        criticalDamage = 1.1;
        isAlive = true;
        exp = rand.nextInt(4)+6;
    }

    @Override
    public boolean isBossMonster() {
        return false;
    }
}
