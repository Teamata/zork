package ooc.zork.Entity.Monsters.NoobMonster;


import ooc.zork.Entity.Combatable;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;

import java.util.Random;

/**
 * Created by Teama on 1/25/2018.
 */

public class Foo extends Monster{

    Random rand = new Random();

    public Foo(){
        attack = rand.nextInt(2)+14;
        defense = rand.nextInt(2)+2;
        maxHP = rand.nextInt(3)+15;
        currentHP = maxHP;
        level = 3;
        speed = 1;
        criticalRate = 0.25;
        criticalDamage = 1.1;
        isAlive = true;
        name = "Foo";
        exp = rand.nextInt(2)+10;
    }

    @Override
    public boolean isBossMonster() {
        return false;
    }
}
