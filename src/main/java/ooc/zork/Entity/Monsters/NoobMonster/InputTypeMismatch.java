package ooc.zork.Entity.Monsters.NoobMonster;

import ooc.zork.Entity.Monsters.Monster;

import java.util.Random;

/**
 * Created by Teama on 1/29/2018.
 */
public class InputTypeMismatch extends Monster {

    Random rand = new Random();

    public InputTypeMismatch() {
        attack = rand.nextInt(2) + 30;
        defense = rand.nextInt(2) + 6;
        maxHP = rand.nextInt(3) + 60;
        currentHP = maxHP;
        level = 25;
        speed = 6;
        criticalRate = 0.25;
        criticalDamage = 1.1;
        isAlive = true;
        name = "InputTypeMismatch";
        exp = rand.nextInt(2) + 25;
    }

    @Override
    public boolean isBossMonster() {
        return false;
    }
}
