package ooc.zork.Entity.Monsters.NoobMonster;

import ooc.zork.Entity.Combatable;
import ooc.zork.Entity.Monsters.Monster;
import ooc.zork.Items.Item;

import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */
public class Memleak extends Monster {

    Random rand = new Random();

    public Memleak(){
        attack = rand.nextInt(5) + 16;
        defense = rand.nextInt(2) + 3;
        maxHP = rand.nextInt(3) + 22;
        currentHP = maxHP;
        level = 12;
        speed = 12;
        criticalRate = 0.25;
        criticalDamage = 1.3;
        isAlive = true;
        name = "Memleak";
        exp = rand.nextInt(2) + 15;
    }

    @Override
    public boolean isBossMonster() {
        return false;
    }
}
