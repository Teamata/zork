package ooc.zork.Entity;

/**
 * Created by Teama on 1/24/2018.
 */
public abstract class Combatable {

    protected int currentHP;
    protected int level;
    protected int maxHP;
    protected int attack;
    protected int speed;
    protected int defense;
    protected double criticalRate;
    protected double criticalDamage;
    protected boolean isAlive;
    protected String name;

    public void setDead(){
        isAlive = false;
    }

    public int getDefense() {
        return defense;
    }

    public int getLevel() {
        return level;
    }

    public double getCriticalDamage() {
        return criticalDamage;
    }

    public double getCriticalRate() {
        return criticalRate;
    }

    public int getAttack() {
        return attack;
    }

    public int getCurrentHP() {
        return currentHP;
    }

    public int getMaxHP() {
        return maxHP;
    }

    public int getSpeed() {
        return speed;
    }

    public String getName() {
        return name;
    }

    public void setCurrentHP(int newHP) {
        this.currentHP = newHP;
        if(currentHP>getMaxHP()){
            this.currentHP = maxHP;
        }
    }

    public void printStats(){
        StringBuilder st = new StringBuilder();
        st.append("Entity name " + this.getName() +"\n"+
                " Level " + this.getLevel()+"\n"+
                " HP " + getMaxHP() +"\n"+
                " Attack " + this.getAttack() +"\n"+
                " Defense " + this.getDefense() +"\n"+
                " Critical rate " + this.getCriticalRate() + "\n"+
                " Critical damage " + this.getCriticalDamage() + "\n"+
                " Speed " + this.getSpeed()+ "\n"
        );
        System.out.println(st);
    }

    public void setAttack(int attack) {
        this.attack = attack;
    }

    public void setCriticalDamage(double criticalDamage) {
        this.criticalDamage = criticalDamage;
    }

    public void setCriticalRate(double criticalRate) {
        this.criticalRate = criticalRate;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public void setMaxHP(int maxHP) {
        this.maxHP = maxHP;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public boolean isDead(){
        return !isAlive;
    }

    public void setAlive(){
        this.isAlive = true;
        this.currentHP = this.maxHP;
    }

    public void setName(String name) {
        this.name = name;
    }
}
