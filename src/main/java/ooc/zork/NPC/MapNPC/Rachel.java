package ooc.zork.NPC.MapNPC;

import ooc.zork.App;
import ooc.zork.NPC.NPC;

/**
 * Created by Teama on 1/29/2018.
 */
public class Rachel implements NPC {

    private final String name = "Rachel the beauty";

    @Override
    public void greeting() {
        System.out.println("Rachel : Don't surprise me like that! This is place is scary enough!!");
    }

    @Override
    public void talk() {
        if(!App.world.getCurrentLevel().getFoundMap()) {
            App.world.getCurrentLevel().setFoundMap(true);
            System.out.println("What?! Fine, I'll share this with you, I've a spare anyway. I give it because looks like you're in need alright?");
            System.out.println("You got floor " + App.world.getCurrentLevel().getLevel() + " map! the unexplored area have been revealed!");
        }
        else{
            System.out.println(".. Don't talk to me, urgh!");
        }
    }

    @Override
    public boolean hasItem() {
        return false;
    }

    @Override
    public boolean isObjective() {
        return false;
    }


    @Override
    public String getName() {
        return name;
    }
}
