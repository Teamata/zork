package ooc.zork.NPC.MapNPC;

import ooc.zork.App;
import ooc.zork.NPC.NPC;

/**
 * Created by Teama on 1/29/2018.
 */
public class Richard implements NPC {
    private final String name = "Richard  de Ricard the First";

    @Override
    public void greeting() {
        System.out.println("Richard : Have you seen my younger brother Ricardo? I ... I just look away for a few seconds and he was gone");
        System.out.println("Richard : Oh.. where ever he is may Arkash bless his safety.");
    }

    @Override
    public void talk() {
        if(!App.world.getCurrentLevel().getFoundMap()) {
            App.world.getCurrentLevel().setFoundMap(true);
            System.out.println("Richard : Looks like you are lost, take this my fellow");
            System.out.println("Richard : You got floor " + App.world.getCurrentLevel().getLevel() + " map! the unexplored area have been revealed!");
        }
        else{
            System.out.println("Richard : I .. don't have anything else. May Arkash bless you");
        }
    }

    @Override
    public boolean hasItem() {
        return false;
    }

    @Override
    public boolean isObjective() {
        return false;
    }


    @Override
    public String getName() {
        return name;
    }
}
