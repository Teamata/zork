package ooc.zork.NPC.MapNPC;

import ooc.zork.App;
import ooc.zork.NPC.NPC;

/**
 * Created by Teama on 1/27/2018.
 */
public class Ricardo implements NPC {

    private final String name = "Ricardo de Ricard the Second";

    @Override
    public void greeting() {
        System.out.println("Ricardo : Hello my fellows, how did you end up in here? What a surprise to see another sane being");
        System.out.println("Ricardo : beside me. May Arkash bless you.");
    }

    @Override
    public void talk() {
        if(!App.world.getCurrentLevel().getFoundMap()) {
            App.world.getCurrentLevel().setFoundMap(true);
            System.out.println("Ricardo : Take this, I no longer need it as I fully explored this place");
            System.out.println("Ricardo : You got floor " + App.world.getCurrentLevel().getLevel() + " map! the unexplored area have been revealed!");
        }
        else{
            System.out.println("Ricardo : I .. don't have anything else. May Arkash bless you");
        }
    }

    @Override
    public boolean hasItem() {
        return false;
    }

    @Override
    public boolean isObjective() {
        return false;
    }


    @Override
    public String getName() {
        return name;
    }
}
