package ooc.zork.NPC.QuestNPC;

import ooc.zork.NPC.NPC;

/**
 * Created by Teama on 1/27/2018.
 */
public class SunUltraSpace implements NPC {

    private final String name = "SunUltraSpace";

    @Override
    public void greeting() {
        System.out.println("I'M S U N U L T RA S P AC E");
    }

    @Override
    public void talk() {
        System.out.println("ALL THE MEMLEAK-- MUST BE ELEMI NA TED");
    }

    @Override
    public boolean hasItem() {
        return false;
    }

    @Override
    public boolean isObjective() {
        return true;
    }

    @Override
    public String getName() {
        return name;
    }
}
