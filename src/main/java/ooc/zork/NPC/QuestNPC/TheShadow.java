package ooc.zork.NPC.QuestNPC;

import ooc.zork.NPC.NPC;

/**
 * Created by Teama on 1/27/2018.
 */
public class TheShadow implements NPC {

    private final String name = "The shadow";

    @Override
    public void greeting() {
        System.out.println("H?a? we M??? b??fore");
    }

    @Override
    public void talk() {
        System.out.println("I have nothing for you");
    }

    @Override
    public boolean hasItem() {
        return false;
    }

    @Override
    public boolean isObjective() {
        return false;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
