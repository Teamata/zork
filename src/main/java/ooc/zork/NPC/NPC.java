package ooc.zork.NPC;

import ooc.zork.World.Level;

/**
 * Created by Teama on 1/24/2018.
 */
public interface NPC {

    void greeting();
    void talk();
    boolean hasItem();
    boolean isObjective();
    String getName();

}
