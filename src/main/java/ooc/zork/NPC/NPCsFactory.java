package ooc.zork.NPC;


import ooc.zork.NPC.MapNPC.Rachel;
import ooc.zork.NPC.MapNPC.Ricardo;
import ooc.zork.NPC.MapNPC.Richard;
import ooc.zork.NPC.QuestNPC.SunUltraSpace;
import ooc.zork.NPC.QuestNPC.TheShadow;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Created by Teama on 1/26/2018.
 */

public class NPCsFactory {


    private static HashMap<Integer,Integer> npcCounts = new HashMap<Integer,Integer>() {
        {
            put(1,0);
            put(2,0);
            put(3,0);
            put(4,0);
        }
    };

    private static final Random rand = new Random();

    private static final HashMap<Integer, Class<? extends NPC>> firstFloorNPCs = new HashMap<Integer, Class<? extends NPC>>() {
        {
            put(0, Ricardo.class);
        }
    };

    private static final HashMap<Integer, Class<? extends NPC>> secondFloorNPCs = new HashMap<Integer, Class<? extends NPC>>() {
        {
            put(0, Richard.class);
        }
    };

    private static final HashMap<Integer, Class<? extends NPC>> thirdFloorNPCs = new HashMap<Integer, Class<? extends NPC>>() {
        {
            put(0, Rachel.class);
        }
    };

    private static final HashMap<Integer, Class<? extends NPC>> fourthFloorNPCs = new HashMap<Integer, Class<? extends NPC>>() {
        {
            put(0, Rachel.class);
        }
    };

    private static final HashMap<Integer, Class<? extends NPC>> objectiveNPCs = new HashMap<Integer,Class<? extends NPC>>(){
        {
            put(0, SunUltraSpace.class);
            put(1, SunUltraSpace.class);
            put(2, TheShadow.class);
            put(3, TheShadow.class);
        }
    };

    private static final HashMap<Integer, Map<Integer , Class<? extends NPC>>> npcs = new HashMap<Integer, Map<Integer, Class<? extends NPC>>>()
    {
        {
            put(1, firstFloorNPCs);
            put(2, secondFloorNPCs);
            put(3, thirdFloorNPCs);
            put(4, fourthFloorNPCs);
        }
    };

    public static NPC getNPC(Integer level) {
        if(npcCounts.get(level) >= npcs.get(level).size()) {
            return null;
        }
        Class<? extends NPC> n = npcs.get(level).get(npcCounts.get(level));
        npcCounts.put(level,npcCounts.get(level)+1);
        NPC npc = null;
        try {
            npc = n.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return npc;
    }

    public static NPC getObjectiveNPC(Integer level){
        Class<? extends NPC> npc = objectiveNPCs.get(level);
        NPC n = null;
        try {
            n = npc.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return n;
    }

    public static int getSize(){
        return npcs.size();
    }
}
